<?php

namespace Drupal\example;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class ExampleController {

  public function hello() {
    return "Hello, there";
  }

  public function helloResponse() {
    return new Response('<html><body>Hello, there</body></html>');
  }

  public function jsonResponse() {
    $data['Hello'] = 'World';
    return new JsonResponse($data);
  }

  public function helloLover($from, $to) {
    return t('<p>Hello, @to, my love</p><p>From, @from</p>',
      array('@to' => $to, '@from' => $from));
  }
}